package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import dto.FrdlistDTO;
import dto.LocationDTO;
import dto.MessageObject;
import dto.PersonDetails;

public class LoginDAO {




	public MessageObject validlogin(Connection con,String username,String password) throws SQLException{

		MessageObject msgObj = new MessageObject();
		/*try
		{*/	ResultSet rs=null;
		//Made changes in select for email;
		PreparedStatement ps = con.prepareStatement("select id,Name,Role_id from usermaster where UserID= ? and Password=?");
		ps.setString(1,username);
		ps.setString(2,password);
		rs = ps.executeQuery();
		if(rs!=null && rs.next())
		{
			msgObj.setId(rs.getString("id"));
			msgObj.setName(rs.getString("Name"));
			msgObj.setRole(rs.getString("Role_id"));
			msgObj.setError("false");
			msgObj.setMessage("Login Successfully");
		}
		else
		{
			msgObj.setError("true");
			msgObj.setMessage("Invalid user");
		}
		//}
		/*catch(Exception e)
		{
			msgObj.setError("true");
			System.err.println("getUser "+e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			try {
				con.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}*/
		return msgObj;
	}


	public ArrayList<PersonDetails> GetRequestedList(Connection con,String userid){

		ArrayList<PersonDetails> p=new ArrayList<PersonDetails>();
		String Photo="";
		try{				

			
			PreparedStatement psc=con.prepareStatement("select UserInvitationMapping.SenderId,UserInvitationMapping.InvitedId," +
					" ProfileMaster.UserId,ProfileMaster.EmailID,ProfileMaster.Photo,ProfileMaster.Name," +
					"ProfileMaster.MobileNo,ProfileMaster.Address,UserInvitationMapping.Status from " +
					"UserInvitationMapping,ProfileMaster where UserInvitationMapping.InvitedId=ProfileMaster.UserId " +
					"and SenderId=?");
			psc.setString(1, userid);

			ResultSet rs=psc.executeQuery();
			
			if (!rs.isBeforeFirst() ) {    
				 System.out.println("No data"); 
				 System.out.println("S111111111111111");

					PreparedStatement psc1=con.prepareStatement("select * from UserInvitationMapping Inner Join ParentMaster on UserInvitationMapping.InvitedId=" +
							"ParentMaster.ParentID where  UserInvitationMapping.id=1");
				//	psc1.setString(1, "505321");
					
					ResultSet rs1=psc1.executeQuery();
					if (rs1!=null) {
						while (rs1.next()) {

							PersonDetails person=new PersonDetails();
							person.setAddress(""+rs1.getString("City"));
							person.setContactNumber(""+rs1.getString("MobileNo"));
							person.setEmailID(""+rs1.getString("EmailID"));
							person.setId(""+rs1.getString("ParentID"));
							person.setName(""+rs1.getString("Name"));
							person.setPhoto(Photo);
							person.setStatus(rs1.getString("status"));
							p.add(person);

						}
					
				}
				 
				}else {
				while (rs.next()) {
				
					
					PersonDetails person=new PersonDetails();
					person.setAddress(""+rs.getString("Address"));
					person.setContactNumber(""+rs.getString("MobileNo"));
					person.setEmailID(""+rs.getString("EmailID"));
					person.setId(""+rs.getString("UserId"));
					person.setName(""+rs.getString("Name"));
					person.setPhoto(""+rs.getString("Photo"));
					person.setStatus(rs.getString("Status"));
					person.toString();
					p.add(person);

				}
			}
			
		
			
		}catch(Exception e){
			e.printStackTrace();
		}
	//	System.out.println("S"+p.toString());
		return p;

	}

	public MessageObject registration(Connection con,String name,String address,String contact,String emailid,String password){
		MessageObject msgo=new MessageObject();
		try{

		//	System.err.print(emailid);
			int result=0;
			PreparedStatement psx=con.prepareStatement("INSERT INTO usermaster(Name,Address,ContactNumber,EmailID,UserID,Password,Role_id) VALUES(?,?,?,?,?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			psx.setString(1, name);
			psx.setString(2, address);
			psx.setString(3, contact);
			psx.setString(4, emailid);
			psx.setString(5, emailid);
			psx.setString(6, password);
			psx.setInt(7, 2);

			result=psx.executeUpdate();

			if (result==0) {
				msgo.setError("true");
				msgo.setMessage("User not resgistered");
			}else{
			//	System.err.println("Error=="+result);
				msgo.setError("false"); 
				msgo.setMessage("User registered Successfully");
			}
		}catch(Exception e){
			msgo.setError("true");
			msgo.setMessage("Error :"+e.getMessage());
			e.printStackTrace();
		}
		return msgo;
	}



	public LocationDTO GetTrackInfo(Connection con,String invited_id){
		LocationDTO  list=new LocationDTO();
     //   System.out.println("GetTrackInfo--dao "+invited_id);

		try{
			PreparedStatement ps=con.prepareStatement("select UserId,Latitude,Longitude,Time,GroupId from UserLocationData where UserId=?");
			ps.setString(1, invited_id);
			ResultSet rs=ps.executeQuery();
			if (rs!=null) {
				while (rs.next()) {

					/*LocationDTO dto=new LocationDTO();*/
					list.setLang(rs.getString("Longitude"));
					list.setLat(rs.getString("Latitude"));
					//list.setPhoto(""+rs.getString("photo"));
					list.setGroupId(rs.getString("GroupId"));
					list.setTime(rs.getString("Time"));
					list.setUserid(rs.getString("UserId"));

				}
			}
		}catch(Exception e){

			e.printStackTrace();
		}
	//	System.err.print("DATA sasa "+list.getLang());
		return list;
	}

	public MessageObject PushLocation(Connection con,String id,String lat,String lan,String time){
		MessageObject  msgo=new MessageObject();
		System.out.println("Location Getting --"+lat+" "+lan+" "+id+" "+time);

		try{/*

			int updateresultset=0;
			String sql="UPDATE usermaster SET latitude= ?,longitude = ?,Time= ? WHERE usermaster.id = ? ";
		//	PreparedStatement ps=con.prepareStatement("Update usermaster set latitude = ? , longitude = ? , Time  = ? where usermaster.id = ?");
			PreparedStatement ps = con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);

			ps.setString(1, lat);
			ps.setString(2, lan);
			ps.setString(3, time);
			ps.setString(4, id);

			updateresultset=ps.executeUpdate();
			System.err.println("---------------"+updateresultset);
			if (updateresultset==0) {

				list.setMessage("Location Not Update");
				list.setError("true");

			}else{
				list.setMessage("Location Update Successfully");
				list.setError("false");

			}
			
		*/
			if(ChekforAvailability(con,id)){
		
			int result=0;
			PreparedStatement psx=con.prepareStatement("INSERT INTO UserLocationData (UserId,Latitude,Longitude,Time) VALUES(?,?,?,GETDATE())",PreparedStatement.RETURN_GENERATED_KEYS);
			psx.setString(1, id);
			psx.setString(2, lat);
			psx.setString(3, lan);
		

			result=psx.executeUpdate();
			psx.clearBatch();
			if (result==0) {
				msgo.setError("true");
				msgo.setMessage("Location not Inserted");
			}else{
			//	System.err.println("Error=="+result);
				msgo.setError("false"); 
				msgo.setMessage("Location Inserted Successfully");
			}
		
		
			}else
			{
				
				int updateresultset=0;
				String sql="UPDATE UserLocationData SET Latitude= ?,Longitude = ?,Time= ? WHERE UserId = ? ";
			//	PreparedStatement ps=con.prepareStatement("Update usermaster set latitude = ? , longitude = ? , Time  = ? where usermaster.id = ?");
				PreparedStatement ps = con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);

				ps.setString(1, lat);
				ps.setString(2, lan);
				ps.setString(3, time);
				ps.setString(4, id);

				updateresultset=ps.executeUpdate();
		//		System.err.println("---------------"+updateresultset);
				if (updateresultset==0) {

					msgo.setMessage("Location Not Update");
					msgo.setError("true");

				}else{
					msgo.setMessage("Location Update Successfully");
					msgo.setError("false");

				}
				
				
			}
		}catch(Exception e){

			msgo.setMessage("Error: "+e.getMessage());
			msgo.setError("true");
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	//	System.err.println(msgo);
		return msgo;
	}


	private boolean ChekforAvailability(Connection con, String id) {

		Boolean valid = null;
		
		try{
			PreparedStatement ps=con.prepareStatement("SELECT Id FROM UserLocationData where UserId= ?");
			ps.setInt(1, Integer.parseInt(id));
			ResultSet rs=ps.executeQuery();
		//	System.out.println("validtesult"+rs.toString()+  rs.getFetchSize());
	
		if (!rs.isBeforeFirst() ) {    
			// System.out.println("No data"); 
				valid= true;

			} else {
				valid= false;

			}
		}catch(Exception e){

			e.printStackTrace();
		}
		
		return valid;
	
	}


	public ArrayList<FrdlistDTO> SearchFriendList(Connection con, Integer id,
			String entity, Integer flag) {
		PreparedStatement ps = null;
		ArrayList<FrdlistDTO>list=new ArrayList<FrdlistDTO>();
		try {
			if( flag==1){
				ps=con.prepareStatement("select ParentMaster.ParentID as Id,ParentMaster.Name,City from ParentMaster where ParentMaster.Name like ? AND ParentID != ?");
				//ps=con.prepareStatement("select Name,id,Address from usermaster where usermaster.Name like '%'"+"'"+entity+"'"+'%'+" AND usermaster.id !="+"'"+id+"'");
		
			}else if (flag==2) {
				ps=con.prepareStatement("select ParentMaster.ParentID as Id,ParentMaster.Name,City from ParentMaster where ParentMaster.ContactNumber like ? AND ParentID!= ?");

			}else if (flag==3) {
				ps=con.prepareStatement("select ParentMaster.ParentID as Id,ParentMaster.Name,City from ParentMaster where ParentMaster.EmailID like ? AND ParentID != ?");

			}
			
			
			
			
			ps.setString(1, "%" + entity + "%");
			ps.setInt(2, id);

			ResultSet rs=ps.executeQuery();
      //     System.out.println("REsult frdb  "+rs+"");
			if (rs!=null) {
				while (rs.next()) {
					FrdlistDTO fd=new FrdlistDTO();
					fd.setName(rs.getString("Name"));
					fd.setUserid(rs.getString("Id"));
					fd.setCity(rs.getString("City"));
					list.add(fd);
				//	System.err.print("DATA  "+list);
				}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
			

	}
	  

	public MessageObject SendInvitation(Connection con, Integer sendId,
			Integer getId) {
	 int invitationlimit=0;
	int invitationcount=0;
	
	MessageObject msgo=new MessageObject();

		try {
			PreparedStatement psc=con.prepareStatement("select InvitationLimit from InvitationMetaData");
			ResultSet rs1=psc.executeQuery();
			if (rs1!=null) {
				while (rs1.next()) {
					invitationlimit=rs1.getInt("InvitationLimit");
				}
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		if (!Valid(con, sendId, getId)) {
			try {
				PreparedStatement psc = con
						.prepareStatement("select InvitationCount from UserInvitationMapping where SenderId=? and InvitedId=?");
				psc.setInt(1, sendId);
				psc.setInt(2, getId);
				ResultSet rs1 = psc.executeQuery();
				if (rs1 != null) {
					while (rs1.next()) {
						invitationcount = rs1.getInt("InvitationCount");
					}
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
		
		if(Valid(con,sendId,getId))
		{
			try{

				int result=0;
				PreparedStatement psx1=con.prepareStatement("INSERT INTO UserInvitationMapping (SenderId,InvitedId,InvitationDate,InvitationCount) VALUES(?,?,GETDATE(),?)",PreparedStatement.RETURN_GENERATED_KEYS);
				psx1.setInt(1, sendId);
				psx1.setInt(2, getId);
				psx1.setInt(3, 1);


				result=psx1.executeUpdate();

				if (result==0) {
					msgo.setError("true");
					msgo.setMessage("Request is not Send");
				}else{
					System.err.println("Error=="+result);
					msgo.setError("false"); 
					msgo.setMessage("Request is sent successfully");
				}
		//		System.out.println("iffffffff=="+result);

			}catch(Exception e){
				msgo.setError("true");
				msgo.setMessage("Error :"+e.getMessage());
				e.printStackTrace();
			}
		}else if(invitationcount<=invitationlimit){
			try{

				int result=0;
				PreparedStatement psx=con.prepareStatement("Update UserInvitationMapping set Status = 1, InvitationDate=GETDATE() ,InvitationCount=? where id = (select id from UserInvitationMapping where SenderId=? and InvitedId=?)");
				psx.setInt(1, invitationcount+1);
				psx.setInt(2, sendId);
				psx.setInt(3, getId);				

				result=psx.executeUpdate();

				if (result==0) {
					msgo.setError("true");
					msgo.setMessage("Request is not Send");
				}else{
					System.err.println("Error=="+result);
					msgo.setError("false"); 
					msgo.setMessage("Request is Send Successfully");
				}
				
		//		System.out.println("Elseiffffffff=="+result);

			}catch(Exception e){
				msgo.setError("true");
				msgo.setMessage("Error :"+e.getMessage());
				e.printStackTrace();
			}
		
		}else {
			msgo.setError("true");
			msgo.setMessage("Your Request  is Overlimit. Please contact Admin(contact@mykiddytracker.com)");
		}
		
		
		return msgo;
	
	}


	private boolean Valid(Connection con, Integer sendId, Integer getId) {
		Boolean valid = null;

		try{
			PreparedStatement ps=con.prepareStatement("select id from UserInvitationMapping where SenderId= ? AND InvitedId= ?");
			ps.setInt(1, sendId);
			ps.setInt(2, getId);
			ResultSet rs=ps.executeQuery();
		//	System.out.println("validtesult"+rs.toString()+  rs.getFetchSize());
	/*		if (rs!=null ||rs.getFetchSize()>0) {
				valid= false;
			}else {
				valid= true;

			}*/
		if (!rs.isBeforeFirst() ) {    
			 System.out.println("No data"); 
				valid= true;

			} else {
				valid= false;

			}
		}catch(Exception e){

			e.printStackTrace();
		}
		
		//System.out.println("Validreq    "+valid);
		return valid;
	}


	public ArrayList<PersonDetails> GetInvitationList(Connection con, String id) {

		ArrayList<PersonDetails> p=new ArrayList<PersonDetails>();
		
		try{			

			
			PreparedStatement psc=con.prepareStatement("select UserInvitationMapping.id," +
					" UserInvitationMapping.SenderId,UserInvitationMapping.InvitedId," +
					"ProfileMaster.UserId,ProfileMaster.EmailID,ProfileMaster.Photo," +
					"ProfileMaster.Name,ProfileMaster.MobileNo,ProfileMaster.Address," +
					"UserInvitationMapping.Status from UserInvitationMapping," +
					"ProfileMaster where UserInvitationMapping.SenderId=ProfileMaster." +
					"UserId and InvitedId=? and UserInvitationMapping.Status=1");
			psc.setString(1, id);
			
		/*	PreparedStatement psc=con.prepareStatement("select * from GetInvitationList where id NOT IN(?) and Role_id NOT IN(1)");
			psc.setString(1, userid);*/
			ResultSet rs=psc.executeQuery();
			if (rs!=null) {
				while (rs.next()) {
				
					PersonDetails person=new PersonDetails();
					person.setAddress(""+rs.getString("Address"));
					person.setContactNumber(""+rs.getString("MobileNo"));
					person.setEmailID(""+rs.getString("EmailID"));
					person.setId(""+rs.getString("UserId"));
					person.setName(""+rs.getString("Name"));
					person.setPhoto(""+rs.getString("Photo"));
					person.setStatus(rs.getString("Status"));
					person.setInvi_Id(rs.getString("id"));

					person.toString();
					
				
					p.add(person);

				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	//	System.out.println("S"+p.toString());
		return p;

	}


	public MessageObject AcceptInvitation(Connection con, String invi_id) {
		// TODO Auto-generated method stub
			MessageObject msgo=new MessageObject();
		
		
			try{

				int result=0;
				PreparedStatement psx=con.prepareStatement("Update UserInvitationMapping set Status =2, AcceptedDate=GETDATE() where id = ?");
				psx.setString(1, invi_id);
				

				result=psx.executeUpdate();

				if (result==0) {
					msgo.setError("true");
					msgo.setMessage("Request is not Accepted");
				}else{
					System.err.println("Error=="+result);
					msgo.setError("false"); 
					msgo.setMessage("Request is Accepted Successfully");
				}
			}catch(Exception e){
				msgo.setError("true");
				msgo.setMessage("Error :"+e.getMessage());
				e.printStackTrace();
			}
		
		
		
		return msgo;
	}


	public LocationDTO Getlocation(Connection con, String invi_id, String user_id) {

		LocationDTO  list=new LocationDTO();
      System.out.println("Getlocation--dao "+invi_id);

		try{
			PreparedStatement ps=con.prepareStatement("select UserId,Latitude,Longitude,Time,GroupId from UserLocationData where UserId=?");
			ps.setString(1, invi_id);
			ResultSet rs=ps.executeQuery();
			if (rs!=null) {
				while (rs.next()) {

					/*LocationDTO dto=new LocationDTO();*/
					list.setLang(rs.getString("Longitude"));
					list.setLat(rs.getString("Latitude"));
					//list.setPhoto(""+rs.getString("photo"));
					list.setGroupId(rs.getString("GroupId"));
					list.setTime(rs.getString("Time"));
					list.setUserid(rs.getString("UserId"));

				}
				
				PreparedStatement ps1=con.prepareStatement("INSERT INTO Frd_Track_Report (TUserId,TrackerId,Lat,Lang,Time,TrackType,Address) VALUES(?,?,?,?,GETDATE(),2,?)",PreparedStatement.RETURN_GENERATED_KEYS);
				ps1.setString(1, user_id);
				ps1.setString(2, list.getUserid());
				ps1.setString(3, list.getLat());
				ps1.setString(4, list.getLang());
				ps1.setString(5, Common.GetAddress( list.getLat(), list.getLang()));

				int rs1=ps1.executeUpdate();
			}
		}catch(Exception e){

			e.printStackTrace();
		}finally{
			
		}
	//	System.err.print("DATA sasa "+list.getLang());
		return list;
	
	}


	public MessageObject RejectInvitation(Connection con, String invi_id) {
		// TODO Auto-generated method stub
		MessageObject msgo=new MessageObject();
	
	
		try{

			int result=0;
			PreparedStatement psx=con.prepareStatement("Update UserInvitationMapping set Status = 3, AcceptedDate=GETDATE() where id = ?");
			psx.setString(1, invi_id);
			

			result=psx.executeUpdate();

			if (result==0) {
				msgo.setError("true");
				msgo.setMessage("Request is not Rejected");
			}else{
		//		System.err.println("Error=="+result);
				msgo.setError("false"); 
				msgo.setMessage("Request is Rejected Successfully");
			}
		}catch(Exception e){
			msgo.setError("true");
			msgo.setMessage("Error :"+e.getMessage());
			e.printStackTrace();
		}
	
	
	
	return msgo;
}


	public ArrayList<PersonDetails> GetAcceptedtrackperson(Connection con, String userid) {

		ArrayList<PersonDetails> p=new ArrayList<PersonDetails>();
		String Photo=null;
		try{				

			
			PreparedStatement psc=con.prepareStatement("select UserInvitationMapping.id, " +
					"UserInvitationMapping.SenderId,UserInvitationMapping.InvitedId," +
					"ProfileMaster.UserId,ProfileMaster.EmailID,ProfileMaster.Photo," +
					"ProfileMaster.Name,ProfileMaster.MobileNo,ProfileMaster.Address," +
					"UserInvitationMapping.Status from UserInvitationMapping,ProfileMaster" +
					" where UserInvitationMapping.SenderId=ProfileMaster.UserId and InvitedId=? and UserInvitationMapping.Status=?");
			psc.setString(1, userid);
			psc.setString(2, "2");

		
			ResultSet rs=psc.executeQuery();
			
			if(rs!=null) {
				while (rs.next()) {
			/*
					 Get Profile Photo From ParentMaster
					 
					 

					PreparedStatement psphoto=con.prepareStatement("select Photo from ProfileMaster where UserId = (select InvitedId from UserInvitationMapping  Where  SenderId = ?)");
					psphoto.setString(1, userid);
					ResultSet rsphoto =psphoto.executeQuery();
					if (rsphoto!=null) {
						while (rsphoto.next()) {
							Photo=rsphoto.getString("Photo");}
					}
					System.out.println("Photo==="+Photo);*/
					
					
					PersonDetails person=new PersonDetails();
					person.setAddress(""+rs.getString("Address"));
					person.setContactNumber(""+rs.getString("MobileNo"));
					person.setEmailID(""+rs.getString("EmailID"));
					person.setId(""+rs.getString("UserId"));
					person.setName(""+rs.getString("Name"));
					person.setPhoto(""+rs.getString("Photo"));
					person.setStatus(rs.getString("Status"));
					person.setInvi_Id(rs.getString("id"));

					person.toString();
					
				
					p.add(person);


				}
			}
			
		
			
		}catch(Exception e){
			e.printStackTrace();
		}
	//	System.out.println("S"+p.toString());
		return p;

	}

	
	public MessageObject BlockTrakRequest(Connection con, String invi_id) {
		// TODO Auto-generated method stub
		MessageObject msgo=new MessageObject();
	
	
		try{

			int result=0;
			PreparedStatement psx=con.prepareStatement("Update UserInvitationMapping set Status = 3, AcceptedDate=GETDATE() where id = ?");
			psx.setString(1, invi_id);
			

			result=psx.executeUpdate();

			if (result==0) {
				msgo.setError("true");
				msgo.setMessage("Request is not Blocked");
			}else{
		//		System.err.println("Error=="+result);
				msgo.setError("false"); 
				msgo.setMessage("Request is Blocked Successfully");
			}
		}catch(Exception e){
			msgo.setError("true");
			msgo.setMessage("Error :"+e.getMessage());
			e.printStackTrace();
		}
	
	
	
	return msgo;
}


	public void PostLocation_Report(Connection con, String invi_id,
			String user_id, String lat, String lang) {
		
		PreparedStatement ps1;
		try {
			ps1 = con.prepareStatement("INSERT INTO Frd_Track_Report (TUserId,TrackerId,Lat,Lang,Time,TrackType,Address) VALUES(?,?,?,?,GETDATE(),1,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			ps1.setString(1, user_id);
			ps1.setString(2, invi_id);
			ps1.setString(3, lat);
			ps1.setString(4, lang);
			ps1.setString(5, Common.GetAddress( lat,lang).toString());

			int result=ps1.executeUpdate();
			
			
			
			if (result==0) {
				System.err.println("Error=="+result);

				System.err.println("Location not Inserted");
			}else{
			System.err.println("Location Inserted Successfully");

			}
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	

}



