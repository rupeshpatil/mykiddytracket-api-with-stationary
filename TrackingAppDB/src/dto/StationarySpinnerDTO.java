package dto;

public class StationarySpinnerDTO {
	String Entityname,EntityId;

	public String getEntityname() {
		return Entityname;
	}

	public void setEntityname(String entityname) {
		Entityname = entityname;
	}

	public String getEntityId() {
		return EntityId;
	}

	public void setEntityId(String entityId) {
		EntityId = entityId;
	}
	
}
