package model;

import java.sql.Connection;
import java.util.ArrayList;

import dao.Database;
import dao.LoginDAO;
import dao.StationarySpinnerdataDAO;
import dto.FrdlistDTO;
import dto.LocationDTO;
import dto.MessageObject;
import dto.PersonDetails;
import dto.StationaryPurchaseItemInfo;
import dto.StationarySpinnerDTO;

public class APIController {
	Connection con=null;
	Database database=null;
	MessageObject msgObj;

	public APIController() {
		database= new Database();
		try
		{
			con = database.getConnection();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}//Login

	public MessageObject validlogin(String  userid,String password) throws Exception {

		try { 
			LoginDAO dao=new LoginDAO();
			msgObj=dao.validlogin(con,userid,password);
		} catch (Exception e) {
			throw e;
		}
		return msgObj;



	}
	public ArrayList<PersonDetails> GetRequestedList(String userid) throws Exception {
		ArrayList<PersonDetails> person=new ArrayList<PersonDetails>();
		try { 

			LoginDAO dao=new LoginDAO();
			person=dao.GetRequestedList(con,userid);
		} catch (Exception e) {
			throw e;
		}
		return person;
	}
	public MessageObject registration(String name,String address,String contact,String emailid,String password) throws Exception {

		try { 
			LoginDAO dao=new LoginDAO();
			msgObj=dao.registration(con,name,address,contact,emailid,password);
		} catch (Exception e) {
			throw e;
		}
		return msgObj;



	}

	public LocationDTO GetTrackInfo(String invited_id) {
		LocationDTO dtp=new LocationDTO();
		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.GetTrackInfo(con,invited_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}
	public MessageObject PushLocation(String id,String lat,String Lan,String time) {
		MessageObject dtp=new MessageObject();
		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.PushLocation(con,id,lat,Lan,time);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public ArrayList<FrdlistDTO> SearchFriendList(Integer id, String entity, Integer flag) {
		ArrayList<FrdlistDTO> dtp=new ArrayList<FrdlistDTO>();
		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.SearchFriendList(con,id,entity,flag);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public MessageObject SendInvitation(Integer sendId, Integer getId) {
		MessageObject dtp=new MessageObject();

		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.SendInvitation(con,sendId,getId);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public ArrayList<PersonDetails> GetInvitationList(String id) {
		ArrayList<PersonDetails> person=new ArrayList<PersonDetails>();
		try { 

			LoginDAO dao=new LoginDAO();
			person=dao.GetInvitationList(con,id);
		} catch (Exception e) {
			throw e;
		}
		return person;
	}

	public MessageObject AcceptInvitation(String invi_id) {
		// TODO Auto-generated method stub
		
		MessageObject dtp=new MessageObject();

		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.AcceptInvitation(con,invi_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public LocationDTO Getlocation(String invi_id, String user_id) throws Exception {
		LocationDTO dtp=new LocationDTO();
		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.Getlocation(con,invi_id,user_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public MessageObject RejectInvitation(String invi_id) {
		// TODO Auto-generated method stub
		
		MessageObject dtp=new MessageObject();

		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.RejectInvitation(con,invi_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public ArrayList<PersonDetails>  GetAcceptedtrackperson(String userid) {
		// TODO Auto-generated method stub
		
		ArrayList<PersonDetails> dtp=new ArrayList<PersonDetails> ();

		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.GetAcceptedtrackperson(con,userid);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public MessageObject BlockTrakRequest(String invi_id) {

		// TODO Auto-generated method stub
		
		MessageObject dtp=new MessageObject();

		try { 
			LoginDAO dao=new LoginDAO();
			dtp=dao.BlockTrakRequest(con,invi_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	
	}

	public void PostLocation_Report(String invi_id, String user_id, String lat, String lang) {
		LocationDTO dtp=new LocationDTO();
		try { 
			LoginDAO dao=new LoginDAO();
			dao.PostLocation_Report(con,invi_id,user_id,lat,lang);
		} catch (Exception e) {
			throw e;
		}
	}

	public ArrayList<StationarySpinnerDTO> GetStationarySpinnerdata(String flag, String id) {
		ArrayList<StationarySpinnerDTO> dtp=new ArrayList<StationarySpinnerDTO>();
		try { 
			StationarySpinnerdataDAO dao=new StationarySpinnerdataDAO();
			dtp=dao.GetStationarySpinnerdata(con,flag,id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public StationaryPurchaseItemInfo GetItemforSubject(String cat_id,
			String subcat_id, String class_id, String subject_id) {
		StationaryPurchaseItemInfo dtp=new StationaryPurchaseItemInfo();

		try { 
			StationarySpinnerdataDAO dao=new StationarySpinnerdataDAO();
			dtp=dao.GetItemforSubject(con,cat_id,subcat_id,class_id,subject_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public StationaryPurchaseItemInfo GetItemforCustomer(String cat_id,
			String subcat_id, String mesure_id, String dimen_id, String color_id) {
		StationaryPurchaseItemInfo dtp=new StationaryPurchaseItemInfo();

		try { 
			StationarySpinnerdataDAO dao=new StationarySpinnerdataDAO();
			dtp=dao.GetItemforCustomer(con,cat_id,subcat_id,mesure_id,dimen_id,color_id);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	public MessageObject SaveStationaryPayment(String userid,
			String totalamount, String paymentmode, String paymentstatus,
			String orderid, String itemtypeid, String quantity,
			String itemamount) {
		MessageObject dtp=new MessageObject();

		try { 
			StationarySpinnerdataDAO dao=new StationarySpinnerdataDAO();
			dtp=dao.SaveStationaryPayment(con,userid, totalamount,paymentmode,paymentstatus,orderid,itemtypeid,quantity,itemamount);
		} catch (Exception e) {
			throw e;
		}
		return dtp;
	}

	


}