package webservices;

import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import model.APIController;

import com.google.gson.Gson;

import dto.FrdlistDTO;
import dto.LocationDTO;
import dto.MessageObject;
import dto.PersonDetails;

@Path("UserServiceAPI")
public class TrackService {

	//login service
	@POST
	@Path("login")
	@Produces("application/json")
	public String validlogin(@FormParam("username") String username,@FormParam("password") String pass)
	{
		String feeds  = null;
		try 
		{

			System.err.print("ffffffffffffffff");
			MessageObject msgData = null;
			APIController handler= new APIController();
			msgData = handler.validlogin(username, pass);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	//login service
	@POST
	@Path("add_user")
	@Produces("application/json")
	public String registration(@FormParam("Name") String name,@FormParam("Address") String address,@FormParam("ContactNumber") String contact,@FormParam("UserID") String emailid,@FormParam("Password") String password)

	{
		String feeds  = null;
		try 
		{


			MessageObject msgData = null;
			APIController handler= new APIController();
			msgData = handler.registration(name, address,contact,emailid,password);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	@POST
	@Path("GetRequestedList")
	@Produces("application/json")
	public String GetRequestedList(@FormParam("userid") String username)
	{
		System.out.println("Nilesh Hit---"+username);

		String feeds  = null;
		try 
		{
			ArrayList<PersonDetails> msgData = new ArrayList<PersonDetails>();
			APIController handler= new APIController();
			msgData = handler.GetRequestedList(username);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}



	@POST
	@Path("GetTrackInfo")
	@Produces("application/json")
	public String GetTrackInfo(@FormParam("InvitedId") String invited_id)
	{
		String feeds  = null;
		try 
		{
			System.err.print("--");
			LocationDTO msgData = new LocationDTO();
			APIController handler= new APIController();
			msgData = handler.GetTrackInfo(invited_id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);
			System.out.println("location data"+feeds);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	
	@POST
	@Path("Getlocation")
	@Produces("application/json")
	public String Getlocation(@FormParam("UserId") String Invi_id,@FormParam("UserId") String user_id)
	{
		String feeds  = null;
		try 
		{
			LocationDTO msgData = new LocationDTO();
			APIController handler= new APIController();
			msgData = handler.Getlocation(Invi_id,user_id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	
	
	@POST
	@Path("PushLocation")
	@Produces("application/json")
	public String PushLocation(@FormParam("id") String id,@FormParam("Lat") String lat,@FormParam("Lang") String lan,@FormParam("Time") String time)
	{
		String feeds  = null;
		try 
		{

			System.err.print("--");
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.PushLocation(id,lat,lan,time);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);
			System.out.println(feeds);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
/*	@POST
	@Path("/PushLocation")
	@Produces("application/json")
	public String Search(@FormParam("id") String id,@FormParam("lat") String lat,@FormParam("lang") String Lan,@FormParam("time") String time)
	{
		String feeds  = null;
		try 
		{
			System.err.print("--");
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.PushLocation(id,lat,Lan,time);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);
			System.out.println(feeds);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}*/
	
	

	@POST
	@Path("/SearchFriendList")
	@Produces("application/json")
	public String SearchFriendList(@FormParam("UserId") Integer id,@FormParam("Entity") String entity,@FormParam("Flag") Integer flag)
	{
		String feeds  = null;
		try 
		{
			System.err.print("--wwwwwww"+id);
			ArrayList<FrdlistDTO> msgData = new ArrayList<FrdlistDTO>();
			APIController handler= new APIController();
			msgData = handler.SearchFriendList(id,entity,flag);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);
			System.out.println(feeds);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}

	
	
	@POST
	@Path("/SendInvitation")
	@Produces("application/json")
	public String SendInvitation(@FormParam("SenderId")Integer SendId,@FormParam("GettingId") Integer GetId)
	{
		String feeds  = null;
		try 
		{
			System.err.print("--");
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.SendInvitation(SendId,GetId);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);
			System.out.println(feeds);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	@POST
	@Path("GetInvitationList")
	@Produces("application/json")
	public String GetInvitationList(@FormParam("userid") String id)
	{
		String feeds  = null;
		try 
		{
			ArrayList<PersonDetails> msgData = new ArrayList<PersonDetails>();
			APIController handler= new APIController();
			msgData = handler.GetInvitationList(id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	@POST
	@Path("AcceptInvitation")
	@Produces("application/json")
	public String AcceptInvitation(@FormParam("Invited_PId") String Invi_id)
	{
		String feeds  = null;
		try 
		{
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.AcceptInvitation(Invi_id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	@POST
	@Path("RejectInvitation")
	@Produces("application/json")
	public String RejectInvitation(@FormParam("Invited_PId") String Invi_id)
	{
		String feeds  = null;
		try 
		{
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.RejectInvitation(Invi_id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	
	
	@POST
	@Path("GetAcceptedtrackperson")
	@Produces("application/json")
	public String GetAcceptedtrackperson(@FormParam("UserId") String userid)
	{
		String feeds  = null;
		try 
		{
			ArrayList<PersonDetails> msgData = new ArrayList<PersonDetails>();
			APIController handler= new APIController();
			msgData = handler.GetAcceptedtrackperson(userid);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	
	@POST
	@Path("BlockTrakRequest")
	@Produces("application/json")
	public String BlockTrakRequest(@FormParam("Invited_Id") String Invi_id)
	{
		String feeds  = null;
		try 
		{
			MessageObject msgData = new MessageObject();
			APIController handler= new APIController();
			msgData = handler.BlockTrakRequest(Invi_id);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	

	@POST
	@Path("PostLocation_Report")
	@Produces("application/json")
	public String PostLocation_Report(@FormParam("TrackpersonId") String Invi_id,@FormParam("UserId") String user_id,
			@FormParam("Lat") String lat,@FormParam("Lang") String lang)
	{
		String feeds  = null;
		try 
		{
			LocationDTO msgData = new LocationDTO();
			APIController handler= new APIController();
			handler.PostLocation_Report(Invi_id,user_id,lat,lang);
			Gson gson = new Gson();
			feeds = gson.toJson(msgData);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return feeds;
	}
	
	
}
